# 7. A/D převodník

Program měří v pravidelných intervalech napětí na potenciometru a hodnotu zobrazuje na displeji. Napětí zobrazujte v levém horním rohu displeje v rozsahu 0-3.3V. Dále napětí zobrazuje graficky jako na osciloskopu.



Implementována  tři řešení:

1. Programové řízení převodníku
2. Převodník s automatickým zahájením převodu
3. Převod na pozadí s využitím přerušení

<img src="adc.png" alt="drawing" width="600" />

[Projekt v MPLab](ADC.X.zip)


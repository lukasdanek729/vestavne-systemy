# 4. Měření kmitočtu

Program na přípravku realizuje měřič kmitočtu. Kmitočet vstupního obdélníkového signálu se střídou 1:1, který je přítomen na pinech RF4/RP10 a RF5/RP17, měřen s využitím čítačových struktur a hodnota kmitočtu v kHz zobrazována na displeji.

 Z technických důvodů je generátor součástí aplikace (je naprogramován v libves.a). Normálně tomu tak není a vstupní kmitočet generuje nějaké vnější zařízení (generátor nebo jiný mikropočítač). Generátor se inicializuje funkcí `**generator_init()**` na začátku programu a funkcí `**generator_run(freqHz)**` se spouští. Parametr freqHz určuje frekvenci na výstupu generátoru. Výstup generátoru je napojen na porty RF4 a RF5 mikropočítače, odkud se kmitočet bere pro tuto úlohu. Mimo to porty RF4/RP10 a RF5/RP17 řídí LED, což je užitečné pro kontrolu, že generátor běží (červená LED svítí).

<img src="mereni.png" alt="drawing" width="600" >

<img src="kmitocet.jpg" alt="drawing" width="600" >

[Projekt v MPLab](KMITOCET.X.zip)

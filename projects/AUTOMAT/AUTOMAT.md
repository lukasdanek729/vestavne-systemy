# 9. Implementace konečného automatu

Na přípravku je implementován automat na vydávání jízdenek na MHD. Jízdenky jsou v hodnotě 6 a 8 Kč. Zákazník nejprve vhodí mince v odpovídající hodnotě a pak stiskne tlačítko jízdenky, kterou chce vydat. Automat vydá maximálně jednu jízdenku, při vyšší částce zbytek peněz vrátí. Do automatu lze vhodit maximálně jednu minci nad limit 8Kč, další mince automat ignoruje (minci okamžitě vrátí). Nákup je možno kdykoliv přerušit tlačítkem „storno“ a automat vrátí částku, odpovídající vhozeným mincím.

Automat je řízen konečným automatem, který přechází ze stavu do stavu při vnějších událostech.

- vhozena 1 Kč
- vhozeny 2 Kč
- vhozeno 5 Kč
- stisknuto „storno“
- stisknuto tlačítko „jízdenka 6 Kč“
- stisknuto tlačítko „jízdenka 8 Kč“

Vrácení mincí je indikováno nápisem na displeji „VYDANO  1 Kc ZPET“. Výdej jízdenky se indikuje nápisem na displeji „VYDANO Jizdne 6Kc“.

Automat se ovládá dvěmi tlačítky touchpadu přípravku. "Šipka dolů - 3" mění výběr v MENU a "Šipka vlevo - 4" potvrzuje aktuální výběr. Ostatní tlačítka touchpadu simulují vházení mincí v hodnotě 1 Kč, 2 Kč a 5 Kč.

Automat je implementován jako Meallyho a to jak tabulkou  přechodů, tak i pomocí příkazu switch. V `main.c` se se dá přepnout, jakou implementaci využít.  



<img src="AUTOMAT.gif" alt="drawing" width="600" />



[Projekt v MPLab](AUTOMAT.X.zip)


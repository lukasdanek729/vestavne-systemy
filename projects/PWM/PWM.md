# 10. Pulzně šířková modulace

Program řídí jas tříbarevné LED diody po složkách R, G, B tak, aby se dala nastavit libovolná barva. Jas jednotlivých složek je řízen pulzně šířkovou modulací a aplikace se ovládá dotykovou klávesnicí. Pro generování pulzně šířkové modulace jsou použity komparační obvody (output compare). 



Hodnota jednotlivých složek se nastavuje v rozmezí 0 - 100% pro co nejjednodušší a dostatečně rychlé řízení jasu, jelikož ovládání pomocí touchpadu je poněkud omezující.  



<img src="PWM.gif" alt="drawing" width="600" />

[Projekt v MPLab](PWM.X.zip)


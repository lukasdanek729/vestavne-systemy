# 6. Komunikace po sériové lince

Program komunikuje s PC přes sériovou linku a pomocí příkazů řídí periferie na přípravku.

Příkazy:

- „&R“ rozsvítí červenou LED,
- „&G“ rozsvítí zelenou LED,
- „&B“ rozsvítí modrou LED,
- „&r“ zhasne červenou LED,
- „&g“ zhasne zelenou LED,
- „&b“ zhasne modrou LED,
- „&s\<xxx\>;“ zobrazí řetězec \<xxx\> na displeji,
- „&i“ pošle do PC text „Pripravek PIC24F Starter Kit;“.



Navíc implementovaná aplikace v C# pro snadnější komunikaci s přípravkem

* Při psaní aplikace využita již implementovaná aplikace z [CREATIVE PROJECT](http://www.caturcreativeproject.com/2017/11/daftar-tutorial-pemrograman-visual-c.html) , která je následně modifikována a doimplementována pro použití s přípravkem.

<img src="uart_aplikace.png" alt="drawing" width="600" >

<img src="uart_pripravek.jpg" alt="drawing" width="600" >

[Projekt v MPLab](UART.X.zip)

[C# projekt Aplikace](UART_app.zip)


# 8. Dotyková klávesnice

Program vypisuje velkými písmeny na displeji číslo stisknutého segmentu dotykové klávesnice. Funkce `touch_getkey` vrátí hodnotu právě stisknuté klávesy (jinak vrací 0) a funkce `touch_readkey` čeká na stisk klávesy a vrací její kód.

Funkce touch_getkey ošetřena proti zákmitům (trojnásobné čtení kláves). Funkce touch_readkey nedovoluje "autorepeat". Žádná z funkcí nevyužívá globální proměnné, ani globální proměnnou předávanou odkazem jako parametr funkce.

Pro demonstraci toho, že touch_readkey nedovolí "autorepeat" se v hlavní smyčce programu upravila na čítač, který se inkrementuje stisknutím jakékoliv klávesy, ale při držení tlačítka se hodnota čítače nepohne. 

<img src="klavesnice.gif" alt="drawing" width="600" />

[Projekt v MPLab](KLAVESNICE.X.zip)

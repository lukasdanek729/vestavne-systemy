# 1. Ovládání LED

Na přípravku realizován zjednodušený semafor, který v pravidelných intervalech co jednu sekundu mění barvu tříbarevné LED takto

1. Červená
2. Oranžová
3. Zelená
4. Oranžová
5. Červená

Zpoždění je provedeno smyčkou s voláním funkce napsané v assembleru, která obsahuje takový počet instrukcí, aby trvala přesně 100 μs. 

<img src="led.gif" alt="drawing" width="600"/>

[Projekt v MPLab](LED.X.zip)

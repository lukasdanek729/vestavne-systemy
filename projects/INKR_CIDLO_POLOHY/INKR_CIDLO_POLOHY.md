# 11. Inkrementační čidlo polohy

Program určuje polohu inkrementálního čidla a data o poloze a rychlosti odesílá po sériové lince do PC. Polohu určujte z impulzů dvou fázově posunutých čidel a index značky prostřednictvím programově implementovaného kvadraturního dekodéru.



Pro tuto úlohu se využívá simulátor inkrementálního čidla. Simulátor na obrazovce nakreslí kruh a čtvereček, který obíhá po obvodu kruhu. Otáčení se ovládá dotykovou klávesnicí. * 4 - rychle protisměru hodinových ručiček * 2 - rychle po směru hodinových ručiček * 1 - krok protisměru hodinových ručiček * 3 - krok po směru hodinových ručiček. Kanál 1 (synchronizační) je napojen na LED B, kanál 2 je napojen na LED G a index značka je na LED R.



<img src="uart_vypis.png" alt="drawing" width="400" />

<img src="INKR_CIDLO_POLOHY.gif" alt="drawing" width="600" />



[Projekt v MPLab](INKR_CIDLO_POLOHY.X.zip)


# 3. Ovládání displeje II

Program zobrazí na displeji dva znaky ve zvětšené podobě tak, jak je naznačeno na obrázku. Jeden bod písmene bude o velikosti 8x8 pixelů (px). Pro vykreslování znaků použitý font, který je součástí knihovny libves.a. Při návrhu programu je snaha minimalizovat počet nutných změn sloupcové adresy při zápisech do obrazové paměti. 

<img src="disp-znaky.png" alt="drawing" width="600"/>






Implementováno navíc: 

* funkce, která dokáže zobrazit textový řetězec delší než dva znaky jako běžící text
* invertování barev (bílé pozadí, černý text nebo obráceně)

<img src="disp2.gif" alt="drawing" width="600"/>

[Projekt v MPLab](DISP2.X.zip)


# 2. Ovládání displeje I

1. S pomocí knihovny VES napsány funkce, které vytisknou na displeji celé číslo v rozsahu datového typu char int, long dekadicky a hexadecimálně, a číslo typu float.

2. S pomocí funkce **`put_pixel`** implementována funkce **`void line(int x1, y1, x2, y2)`**, která nakreslí čáru.

3. Funkce **`line`** použita pro vytvoření funkce pro nakreslení polygonu **`void poly(int n, int\* coord)`**, kde n je počet vrcholů polygonu a coord je pole celých čísel reprezentující po řadě souřadnice bodů x0, y0, x1, y1, …

4. Implementace funkce **` void graphic_demo()`** , ve které využívá funkcí knihovny, včetně grafických funkcí

   <img src="disp1.gif" alt="drawing" width="450"/>

[Projekt v MPLab](DISP1.X.zip)

# Vestavné systémy

Projekt obsahuje úlohy z předmětu Vestavné systémy (BI-VES). Popisky implementovaných úloh jsou převzaty ze zadání BI-VES na FIT ČVUT, který zaštiťuje Ing. Miroslav Skrbek, Ph.D.

## HW Přípravek

<img src="pripravek.png" alt="drawing" width="500"/>

Aplikační část přípravku (vpravo od bílé dělící čáry) obsahuje mikrokontrolér PIC24FJ256GB106 (1), ke kterému je připojen displej (3), kapacitní dotyková klávesnice (7), tříbarevná RGB LED dioda (11), potenciometr (6), USB konektor typ A: host, On-The-Go (4) a USB konektor B - device (5). Mikrokontrolér (1) je programován programátorem, který se nachází vlevo od bílé dělící čáry. Programátor obsahuje mikrokontrolér (8) s USB rozhraním a s počítačem komunikuje přes konektor (2). Mikrokotrolér (1) běží na hodinovém kmitočtu 12MHz, který získává z programátoru přes externí hodninový vstup. Krystal 32768Hz (10) je určen pro realizaci hodin reálného času a je napojen na mikrokontrolér (1).

[Stránky výrobce pro MPLAB Starter Kit for PIC24F MCUs](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/DM240011)

#### Data Sheets

 [PIC24FJ256GB110 Family Data Sheet](pic24fj256gb110.pdf)

 [PIC24F Family Reference Manual, Sect. 14 Timers](pic24f-timers.pdf)

 [PIC24F Family Ref. Manual, Sect. 21 UART](pic24f-uart.pdf)

 [PIC24F Family Reference Manual, Sect. 17 10-Bit A/D Converter](pic24f-ad.pdf)

 [PIC24F Family Reference Manual, Sect. 11 Charge Time Measurement Unit (CTMU)](pic24f-ctmu.pdf)

 [AN1250, Microchip CTMU for Capacitive Touch Applications](touch-an1250.pdf)

[PIC24F Family Reference Manual, Sect. 16 Output Compare](pic24f-output-compare.pdf)

#### Vývojové prostředí a kompilátor

[MPLAB® X Integrated Development Environment (IDE)](https://www.microchip.com/en-us/development-tools-tools-and-software/mplab-x-ide)

[MPLAB® XC Compilers](https://www.microchip.com/en-us/development-tools-tools-and-software/mplab-xc-compilers) - XC16 Compiler

#### Dodaná knihovna pro práci na úlohách

Pro jednodušší implementaci některých úloh či simulaci požadovaných podmínek se v úlohách používá *libves.a* vytvořena přímo pro předmět BI-VES vedoucím předmětu.

[Knihovna VES (hlavičkové soubory a libves.X.a)](libves.x.zip)

## Seznam úloh

1. [Ovládání LED](projects/LED/LED.md)
2. [Ovládání displeje 1](projects/DISP1/DISP1.md)
3. [Ovládání displeje 2](projects/DISP2/DISP2.md)
4. [Měření kmitočtu](projects/KMITOCET/KMITOCET.md)
5. [Měření periody](projects/PERIODA/PERIODA.md)
6. [Komunikace po sériové lince](projects/UART/UART.md)
7. [A/D převodník](projects/ADC/ADC.md)
8. [Dotyková klávesnice](projects/KLAVESNICE/KLAVESNICE.md)
9. [Implementace konečného automatu](projects/AUTOMAT/AUTOMAT.md)
10. [Pulzně šířková modulace](projects/PWM/PWM.md)
11. [Inkrementační čidlo polohy](projects/INKR_CIDLO_POLOHY/INKR_CIDLO_POLOHY.md)
